"""Functions for assignment 1 for this Artificial Intelligence course"""

"""Function to return cube of n """
def cube(n):
	return n*n*n

"""Function to calc factorial of n"""
def factorial(n):
	if(n<0 or type(n) is not int):
		raise ValueError('n must be a +ve integer or 0')
	else:
		if(n==0):
			return 1
		else:
			nfac=n
			for i in range(1,n):
				nfac=nfac*i
			return nfac
"""Function to count number of times 'pattern' appears
in 'lst' """
def count_pattern(pattern, lst):
	#if same list twice
	if(pattern==lst):
		return 1
		#raise RuntimeError('Supplied same list twice!') 
	elif(len(pattern)>=len(lst)):
		raise RuntimeError('Pattern should be smaller than list!')
	pattLength=len(pattern)
	#instances of pattern
	nPatt=0
	#loop over lst look for first piece of pattern
	for i in range(0,len(lst)):
		if (lst[i]==pattern[0]):
			#have found first piece of pattern,
			#look until we see rest or don't
			#bool=false if pattern breaks.
			pattBool=True
			for j in range(1,pattLength):
				#if don't see next piece of pattern
				if(lst[i+j]!=pattern[j]):
					pattBool=False
					break
			#if we get here without pattBool=false then pattern has been matched
			if(pattBool):
				nPatt+=1
			
	return nPatt	
"""Calc depth of expression based upon depth of nested lists/tuples"""
def depth(expression):
	Depth=0
	#first check if expression itself isn't a tuple
	if not isinstance(expression,(list,tuple)):
		return 0
	#list is a tuple so apply depth to all elements
	#of expression - map(func,list) does this -
	#you want the maximum returned by this +1 
	#(+1 because the fact you have a tuple is one layer
	#of depth)....
	return max(map(depth,expression))+1	

"""
#Tests!
print depth(('+', ('expt', 'x', 2), ('expt', 'y', 2))) 
print depth(('expt', 'x', 2))
print depth(('/', ('expt', 'x', 5), ('expt', ('-', ('expt', 'x', 2),
1), ('/', 5, 2))))
"""
